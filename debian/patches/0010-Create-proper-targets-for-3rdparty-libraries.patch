From: =?utf-8?q?Timo_R=C3=B6hling?= <timo.roehling@fkie.fraunhofer.de>
Date: Sat, 14 Mar 2020 13:38:46 +0100
Subject: Create proper targets for 3rdparty libraries

---
 CMakeLists.txt                                   | 142 +++++++++++------------
 src/Open3D/CMakeLists.txt                        | 127 +++-----------------
 src/Open3D/Camera/CMakeLists.txt                 |   2 +
 src/Open3D/ColorMap/CMakeLists.txt               |   2 +
 src/Open3D/Geometry/CMakeLists.txt               |   2 +
 src/Open3D/IO/CMakeLists.txt                     |   9 +-
 src/Open3D/Integration/CMakeLists.txt            |   2 +
 src/Open3D/Odometry/CMakeLists.txt               |   2 +
 src/Open3D/Open3DConfig.cmake.in                 |  44 +++++--
 src/Open3D/Registration/CMakeLists.txt           |   2 +
 src/Open3D/Utility/CMakeLists.txt                |   2 +
 src/Open3D/Visualization/CMakeLists.txt          |   2 +
 src/Tools/CMakeLists.txt                         |  10 +-
 src/Tools/ManuallyAlignPointCloud/CMakeLists.txt |   2 +-
 src/UnitTest/CMakeLists.txt                      |   2 +-
 15 files changed, 140 insertions(+), 212 deletions(-)

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 43ea0ac..f30e61b 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -3,7 +3,6 @@
 # https://gitlab.kitware.com/cmake/community/wikis/doc/tutorials/
 
 cmake_minimum_required(VERSION 3.1)
-set (CMAKE_CXX_STANDARD 11)
 include(GNUInstallDirs)
 
 add_custom_target(build_all_3rd_party_libs
@@ -62,8 +61,6 @@ endif()
 set(INSTALL_CMAKE_DIR ${DEF_INSTALL_CMAKE_DIR} CACHE PATH
     "Installation directory for CMake files")
 
-set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${PROJECT_SOURCE_DIR}/3rdparty/CMake)
-
 set_property(GLOBAL PROPERTY USE_FOLDERS ON)
 
 # config options
@@ -104,8 +101,6 @@ if (ENABLE_HEADLESS_RENDERING)
     if (APPLE)
         message(FATAL_ERROR "Headless rendering is not supported on Mac OS")
         set(ENABLE_HEADLESS_RENDERING OFF)
-    else()
-        add_definitions(-DHEADLESS_RENDERING)
     endif()
 endif()
 
@@ -172,15 +167,6 @@ elseif (APPLE)
     endif()
 elseif (UNIX)
     add_definitions(-DUNIX)
-    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -fPIC")
-    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fPIC")
-    add_compile_options(-Wno-deprecated-declarations)
-    add_compile_options(-Wno-unused-result)
-    # In Release build -O3 will be added automatically by CMake
-    # We still enable -O3 at debug build to optimize performance
-    if (uppercase_CMAKE_BUILD_TYPE STREQUAL "DEBUG")
-        add_definitions(-O3)
-    endif()
     # disable BUILD_LIBREALSENSE since it is not fully supported on Linux
     message(STATUS "Compiling on Unix")
     message(STATUS "Disable RealSense since it is not fully supported on Linux.")
@@ -189,35 +175,8 @@ endif ()
 
 # Set OpenMP
 if (WITH_OPENMP)
-    find_package(OpenMP QUIET)
-    if (OPENMP_FOUND)
-        message(STATUS "Using installed OpenMP ${OpenMP_VERSION}")
-        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
-        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
-        set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${OpenMP_EXE_LINKER_FLAGS}")
-
-        set(Config_Open3D_C_FLAGS          ${OpenMP_C_FLAGS})
-        set(Config_Open3D_CXX_FLAGS        ${OpenMP_CXX_FLAGS})
-        set(Config_Open3D_EXE_LINKER_FLAGS ${OpenMP_EXE_LINKER_FLAGS})
-    else ()
-        message(STATUS "OpenMP NOT found")
-    endif ()
-  endif ()
-
-# recursively parse and return the entire directory tree.
-# the result is placed in output
-function(Directories root output)
-    set(data "")
-    list(APPEND data ${root})
-    file(GLOB_RECURSE children LIST_DIRECTORIES true "${root}/*")
-    list(SORT children)
-    foreach(child ${children})
-        if (IS_DIRECTORY ${child})
-            list(APPEND data ${child})
-        endif()
-    endforeach()
-    set (${output} ${data} PARENT_SCOPE)
-endfunction()
+    find_package(OpenMP)
+endif ()
 
 macro(ADD_SOURCE_GROUP MODULE_NAME)
     file(GLOB MODULE_HEADER_FILES "${MODULE_NAME}/*.h")
@@ -226,41 +185,74 @@ macro(ADD_SOURCE_GROUP MODULE_NAME)
     source_group("Source Files\\${MODULE_NAME}" FILES ${MODULE_SOURCE_FILES})
 endmacro(ADD_SOURCE_GROUP)
 
-# 3rd-party projects that are added with external_project_add will be installed
-# with this prefix. E.g.
-# - 3RDPARTY_INSTALL_PREFIX: Open3D/build/3rdparty_install
-# - Headers: Open3D/build/3rdparty_install/include/extern_lib.h
-# - Libraries: Open3D/build/3rdparty_install/lib/extern_lib.a
-set(3RDPARTY_INSTALL_PREFIX "${CMAKE_BINARY_DIR}/3rdparty_install")
-
-# 3rd-party libraries using the ExternalProject_Add approach will install
-# headers in Open3D/build/3rdparty_install/include. We prioritize this include
-# directory by putting it in front, to avoid mistakenly including other header
-# files of the same name.
-include_directories(${3RDPARTY_INSTALL_PREFIX}/include)
-
-# 3rd-party libraries using the ExternalProject_Add approach will install
-# libs in Open3D/build/3rdparty_install/lib.
-# This isn't required for Ubuntu/Mac since the link directory info is propagated
-# with the interface library. We still need this for Windows.
-link_directories(${3RDPARTY_INSTALL_PREFIX}/lib)
-
-# Handling dependencies
-add_subdirectory(3rdparty)
-link_directories(${3RDPARTY_LIBRARY_DIRS})
-
-# set include directories
-include_directories(
-    ${PROJECT_SOURCE_DIR}/src
+# The debianized sources have CMake modules to use third-party libraries
+list(INSERT CMAKE_MODULE_PATH 0 "${CMAKE_SOURCE_DIR}/debian/cmake-modules")
+find_package(Eigen3 REQUIRED)
+find_package(GLEW REQUIRED)
+find_package(JPEG REQUIRED)
+add_library(3rdparty::JPEG INTERFACE IMPORTED)
+set_target_properties(3rdparty::JPEG PROPERTIES
+	INTERFACE_INCLUDE_DIRECTORIES ${JPEG_INCLUDE_DIR}
+	INTERFACE_LINK_LIBRARIES ${JPEG_LIBRARIES}
 )
+find_package(PNG REQUIRED)
+find_package(flann_cpp REQUIRED)
+find_package(fmt REQUIRED)
+find_package(glfw3 REQUIRED)
+find_package(googletest REQUIRED)
+find_package(jsoncpp REQUIRED)
+find_package(liblzf REQUIRED)
+find_package(pybind11 REQUIRED)
+find_package(Qhull REQUIRED)
+find_package(TinyGLTF REQUIRED)
+find_package(tinyobjloader REQUIRED)
+# We need to build those from source
+add_library(rply STATIC 3rdparty/rply/rply.c)
+target_include_directories(rply SYSTEM PUBLIC 3rdparty)
+set_target_properties(rply PROPERTIES POSITION_INDEPENDENT_CODE ON)
+add_library(tinyfiledialogs STATIC 3rdparty/tinyfiledialogs/tinyfiledialogs.c)
+target_include_directories(tinyfiledialogs SYSTEM PUBLIC 3rdparty)
+set_target_properties(tinyfiledialogs PROPERTIES POSITION_INDEPENDENT_CODE ON)
 
-# Suppress 3rdparty header warnings with SYSTEM
-include_directories(
-    SYSTEM
-    ${PROJECT_SOURCE_DIR}/3rdparty
-    ${3RDPARTY_INCLUDE_DIRS}
-    ${PROJECT_SOURCE_DIR}/3rdparty/librealsense/include
+set(OPEN3D_3RDPARTY_LIBS "Eigen3::Eigen;GLEW::GLEW;glfw;fmt::fmt;3rdparty::flann_cpp;3rdparty::JPEG;PNG::PNG;jsoncpp_lib;liblzf::liblzf;Qhull::qhullcpp;rply;tinyfiledialogs;TinyGLTF::TinyGLTF;tinyobjloader::tinyobjloader")
+if(TARGET OpenMP::OpenMP_CXX)
+	list(APPEND OPEN3D_3RDPARTY_LIBS OpenMP::OpenMP_CXX)
+endif()
+# No Azure Kinect
+set(BUILD_AZURE_KINECT_COMMENT "//")
+# External dependencies we need to find in the config script
+set(OPEN3D_3RDPARTY_PUBLIC_MODULES "Eigen3;fmt;GLEW;glfw3")
+# External library headers we need (but no linking against said libraries)
+set(OPEN3D_3RDPARTY_PUBLIC_HEADERS "GLEW::GLEW;glfw;fmt::fmt")
+# Exernal libraries we need as part of our interface
+set(OPEN3D_3RDPARTY_PUBLIC_LIBS "Eigen3::Eigen")
+add_library(HeaderDepends INTERFACE)
+foreach(target IN LISTS OPEN3D_3RDPARTY_PUBLIC_HEADERS)
+	get_property(inc TARGET ${target} PROPERTY INTERFACE_INCLUDE_DIRECTORIES)
+	target_include_directories(HeaderDepends SYSTEM INTERFACE ${inc})
+endforeach()
+add_library(LibDepends INTERFACE)
+target_compile_options(LibDepends INTERFACE -Wno-deprecated-declarations -Wno-unused-result)
+target_compile_features(LibDepends INTERFACE cxx_constexpr cxx_variadic_templates cxx_template_template_parameters)
+target_link_libraries(LibDepends INTERFACE ${OPEN3D_3RDPARTY_LIBS})
+target_include_directories(LibDepends INTERFACE
+	${CMAKE_SOURCE_DIR}/src
 )
+target_include_directories(LibDepends SYSTEM INTERFACE
+	${CMAKE_SOURCE_DIR}/3rdparty
+	${CMAKE_SOURCE_DIR}
+)
+# headless rendering
+if (ENABLE_HEADLESS_RENDERING)
+    find_package(OSMesa REQUIRED)
+    target_include_directories(LibDepends INTERFACE ${OSMESA_INCLUDE_DIR})
+    target_link_libraries(LibDepends INTERFACE ${OSMESA_LIBRARY})
+    target_compile_definitions(LibDepends INTERFACE HEADLESS_RENDERING)
+else ()
+    find_package(OpenGL REQUIRED COMPONENTS GLX)
+    target_link_libraries(LibDepends INTERFACE OpenGL::OpenGL)
+endif ()
+
 
 # Open3D library
 add_subdirectory(src)
diff --git a/src/Open3D/CMakeLists.txt b/src/Open3D/CMakeLists.txt
index 805c709..271490c 100644
--- a/src/Open3D/CMakeLists.txt
+++ b/src/Open3D/CMakeLists.txt
@@ -55,118 +55,22 @@ set_target_properties(${CMAKE_PROJECT_NAME} PROPERTIES
 	SOVERSION "0d"
 )
 
-# headless rendering
-if (ENABLE_HEADLESS_RENDERING)
-    find_package(OSMesa REQUIRED)
-    include_directories(${OSMESA_INCLUDE_DIR})
-    target_link_libraries(${CMAKE_PROJECT_NAME} ${OSMESA_LIBRARY})
-else ()
-    find_package(OpenGL REQUIRED)
-    target_link_libraries(${CMAKE_PROJECT_NAME} ${OPENGL_LIBRARIES})
-endif ()
-
-target_link_libraries(${CMAKE_PROJECT_NAME}
-                      ${3RDPARTY_LIBRARIES}
-                      ${OMP_LIBRARIES})
-
-# input_dirs: a list of absolute paths in the source dir
-# output_dirs: a list of relative paths in the install dir
-# input_base_dir: the base dir of input_dirs
-function(SourcePath2InstallPath input_dirs output_dirs input_base_dir)
-    get_filename_component(absolute_input_base_dir ${input_base_dir} ABSOLUTE)
-    foreach(input_dir ${input_dirs})
-        # We need to handle case where input_dir is not child subdirectory of
-        # input_base_dir (or input_base_dir itself).
-        # This could happen for pre-installed headers, e.g.
-        #   input_dir == /usr/include/libpng16
-        #   input_base_dir = /path-to-repo/Open3D
-        if(${input_dir} MATCHES "^${absolute_input_base_dir}($|/.*)")
-            # extract path relative to the project source dir
-            set(relative_path "")
-            file(RELATIVE_PATH relative_path ${absolute_input_base_dir} ${input_dir})
-
-            # construct relative path in the install dir
-            set(install_path "${CMAKE_INSTALL_PREFIX}/include/${CMAKE_PROJECT_NAME}/${relative_path}")
-
-            # save
-            list(APPEND converted ${install_path})
-        else()
-            # When input_dir is not child subdirectory of input_base_dir,
-            # we simply use the input_dir.
-            list(APPEND converted ${input_dir})
-        endif()
-    endforeach()
-    set (${output_dirs} ${converted} PARENT_SCOPE)
-endfunction()
-
-# build a list of include folders
-SourcePath2InstallPath("${3RDPARTY_INCLUDE_DIRS_AT_INSTALL}" INSTALL_3RDPARTY_INCLUDE_DIRS ${PROJECT_SOURCE_DIR})
-
-# set Open3D include directories
-list(APPEND CONFIG_Open3D_INCLUDE_DIRS
-    ${CMAKE_INSTALL_PREFIX}/include
-    ${INSTALL_3RDPARTY_INCLUDE_DIRS}
+target_link_libraries(${CMAKE_PROJECT_NAME} PRIVATE LibDepends INTERFACE ${OPEN3D_3RDPARTY_PUBLIC_LIBS})
+target_compile_features(${CMAKE_PROJECT_NAME}
+	INTERFACE cxx_constexpr cxx_variadic_templates
+)
+target_include_directories(${CMAKE_PROJECT_NAME} INTERFACE
+	$<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/src>
+	$<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>
 )
-list(REMOVE_DUPLICATES CONFIG_Open3D_INCLUDE_DIRS)
-
-
-# set Open3D libraries
-# If Open3D is a shared library
-#     - Built-from-source 3rd-party libraries is embedded in Open3D
-#     - Built-from-source 3rd-party libraries will NOT be installed
-#     - A user app needs to link
-#           1) Open3D
-#           2) Pre-installed 3rd-party libraries
-# If Open3D is a static library
-#     - Built-from-source 3rd-party libraries is NOT embedded in Open3D
-#     - Built-from-source 3rd-party libraries will be installed
-#     - A user app needs to link
-#           1) Open3D
-#           2) Pre-installed 3rd-party libraries
-#           3) Built-from-source 3rd-party libraries
-list(APPEND CONFIG_Open3D_LIBRARIES "${CMAKE_PROJECT_NAME}" ${OMP_LIBRARIES})
-if (BUILD_SHARED_LIBS)
-    list(APPEND CONFIG_Open3D_LIBRARIES ${PRE_BUILT_3RDPARTY_LIBRARIES})
-else ()
-    list(APPEND CONFIG_Open3D_LIBRARIES ${3RDPARTY_LIBRARIES})
-endif ()
-
-# hot-fix for glfw vs glfw3 issue for Windows
-# TODO: we shall revisit this for a better fix
-# glfw is the target_name i.e. there is add_library(glfw) in glfw's cmake
-# glfw3.lib (or libglfw3.a) is the library name of the compiled glfw
-function(list_replace src_list dst_list src_val dst_val)
-    foreach(val ${src_list})
-        if(${val} STREQUAL ${src_val})
-            message(STATUS "Replacing ${val} with ${dst_val}")
-            list(APPEND converted ${dst_val})
-        else()
-            list(APPEND converted ${val})
-        endif()
-    endforeach()
-    set(${dst_list} ${converted} PARENT_SCOPE)
-endfunction(list_replace)
-
-if (${BUILD_GLFW})
-    message(STATUS "${CONFIG_Open3D_LIBRARIES}")
-    list_replace("${CONFIG_Open3D_LIBRARIES}" CONFIG_Open3D_LIBRARIES "glfw" "glfw3")
-    message(STATUS "${CONFIG_Open3D_LIBRARIES}")
-endif()
-
-# set Open3D library directories
-list(APPEND CONFIG_Open3D_LIBRARY_DIRS
-    ${CMAKE_INSTALL_LIBDIR}
-    ${3RDPARTY_LIBRARY_DIRS}
-    ${OMP_LIBRARY_DIRS})
 
 include(CMakePackageConfigHelpers)
 
 # find_package Open3D
 configure_package_config_file(Open3DConfig.cmake.in
                               "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/Open3DConfig.cmake"
+			      PATH_VARS CMAKE_INSTALL_INCLUDEDIR
                               INSTALL_DESTINATION ${INSTALL_CMAKE_DIR}
-                              PATH_VARS CONFIG_Open3D_INCLUDE_DIRS CONFIG_Open3D_LIBRARY_DIRS
-                              NO_SET_AND_CHECK_MACRO
                               NO_CHECK_REQUIRED_COMPONENTS_MACRO)
 
 # find_package Open3D Version
@@ -175,7 +79,7 @@ write_basic_package_version_file("${PROJECT_BINARY_DIR}/Open3DConfigVersion.cmak
                                  COMPATIBILITY ExactVersion)
 
 # install
-install(TARGETS ${CMAKE_PROJECT_NAME}
+install(TARGETS ${CMAKE_PROJECT_NAME} EXPORT ${CMAKE_PROJECT_NAME}Targets
 	RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
 	LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
 	ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR})
@@ -189,12 +93,9 @@ install(FILES
         "${PROJECT_BINARY_DIR}/Open3DConfigVersion.cmake"
         DESTINATION "${INSTALL_CMAKE_DIR}" COMPONENT dev)
 
-# uninstall target
-if (NOT TARGET uninstall)
-    configure_file("${CMAKE_CURRENT_SOURCE_DIR}/cmake_uninstall.cmake.in"
-                   "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
-                   IMMEDIATE @ONLY)
+install(EXPORT ${CMAKE_PROJECT_NAME}Targets
+	NAMESPACE Open3D::
+	DESTINATION ${INSTALL_CMAKE_DIR} COMPONENT dev)
+
+export(EXPORT ${CMAKE_PROJECT_NAME}Targets)
 
-    add_custom_target(uninstall COMMAND ${CMAKE_COMMAND} -P
-                      ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake)
-endif ()
diff --git a/src/Open3D/Camera/CMakeLists.txt b/src/Open3D/Camera/CMakeLists.txt
index ab2bb4b..a425050 100644
--- a/src/Open3D/Camera/CMakeLists.txt
+++ b/src/Open3D/Camera/CMakeLists.txt
@@ -3,6 +3,8 @@ file(GLOB_RECURSE ALL_SOURCE_FILES "*.cpp")
 
 # create object library
 add_library(Camera OBJECT ${ALL_SOURCE_FILES})
+set_target_properties(Camera PROPERTIES POSITION_INDEPENDENT_CODE ON)
+target_link_libraries(Camera PRIVATE LibDepends)
 ShowAndAbortOnWarning(Camera)
 
 # Enforce 3rd party dependencies
diff --git a/src/Open3D/ColorMap/CMakeLists.txt b/src/Open3D/ColorMap/CMakeLists.txt
index 06e7f28..7c0f133 100644
--- a/src/Open3D/ColorMap/CMakeLists.txt
+++ b/src/Open3D/ColorMap/CMakeLists.txt
@@ -3,6 +3,8 @@ file(GLOB_RECURSE ALL_SOURCE_FILES "*.cpp")
 
 # create object library
 add_library(ColorMap OBJECT ${ALL_SOURCE_FILES})
+set_target_properties(ColorMap PROPERTIES POSITION_INDEPENDENT_CODE ON)
+target_link_libraries(ColorMap PRIVATE LibDepends)
 ShowAndAbortOnWarning(ColorMap)
 
 # Enforce 3rd party dependencies
diff --git a/src/Open3D/Geometry/CMakeLists.txt b/src/Open3D/Geometry/CMakeLists.txt
index e32386a..cefadea 100644
--- a/src/Open3D/Geometry/CMakeLists.txt
+++ b/src/Open3D/Geometry/CMakeLists.txt
@@ -3,6 +3,8 @@ file(GLOB_RECURSE ALL_SOURCE_FILES "*.cpp")
 
 # create object library
 add_library(Geometry OBJECT ${ALL_SOURCE_FILES})
+set_target_properties(Geometry PROPERTIES POSITION_INDEPENDENT_CODE ON)
+target_link_libraries(Geometry PRIVATE LibDepends)
 ShowAndAbortOnWarning(Geometry)
 
 # Enforce 3rd party dependencies
diff --git a/src/Open3D/IO/CMakeLists.txt b/src/Open3D/IO/CMakeLists.txt
index b1c8bdf..d712664 100644
--- a/src/Open3D/IO/CMakeLists.txt
+++ b/src/Open3D/IO/CMakeLists.txt
@@ -3,9 +3,6 @@ file(GLOB_RECURSE CLASS_IO_SOURCE_FILES "ClassIO/*.cpp")
 file(GLOB_RECURSE FILE_FORMAT_SOURCE_FILES "FileFormat/*.cpp")
 set(IO_ALL_SOURCE_FILES ${CLASS_IO_SOURCE_FILES} ${FILE_FORMAT_SOURCE_FILES})
 
-file(GLOB         RPLY_SOURCE_FILES "../../../3rdparty/rply/*.c")
-file(GLOB         LIBLZF_SOURCE_FILES "../../../3rdparty/liblzf/*.c")
-
 if (BUILD_AZURE_KINECT)
     file(GLOB_RECURSE SENSOR_SOURCE_FILES "Sensor/*.cpp")
     set(IO_ALL_SOURCE_FILES ${IO_ALL_SOURCE_FILES} ${SENSOR_SOURCE_FILES})
@@ -13,9 +10,9 @@ endif ()
 
 # Create object library
 add_library(IO OBJECT
-            ${IO_ALL_SOURCE_FILES}
-            ${RPLY_SOURCE_FILES}
-            ${LIBLZF_SOURCE_FILES})
+            ${IO_ALL_SOURCE_FILES})
+set_target_properties(IO PROPERTIES POSITION_INDEPENDENT_CODE ON)
+target_link_libraries(IO PRIVATE LibDepends)
 ShowAndAbortOnWarning(IO)
 
 # Enforce 3rd party dependencies
diff --git a/src/Open3D/Integration/CMakeLists.txt b/src/Open3D/Integration/CMakeLists.txt
index 2e097cd..3e372dc 100644
--- a/src/Open3D/Integration/CMakeLists.txt
+++ b/src/Open3D/Integration/CMakeLists.txt
@@ -3,6 +3,8 @@ file(GLOB_RECURSE ALL_SOURCE_FILES "*.cpp")
 
 # create object library
 add_library(Integration OBJECT ${ALL_SOURCE_FILES})
+set_target_properties(Integration PROPERTIES POSITION_INDEPENDENT_CODE ON)
+target_link_libraries(Integration PRIVATE LibDepends)
 ShowAndAbortOnWarning(Integration)
 
 # Enforce 3rd party dependencies
diff --git a/src/Open3D/Odometry/CMakeLists.txt b/src/Open3D/Odometry/CMakeLists.txt
index 2d782ed..9e9a675 100644
--- a/src/Open3D/Odometry/CMakeLists.txt
+++ b/src/Open3D/Odometry/CMakeLists.txt
@@ -3,6 +3,8 @@ file(GLOB_RECURSE ALL_SOURCE_FILES "*.cpp")
 
 # create object library
 add_library(Odometry OBJECT ${ALL_SOURCE_FILES})
+set_target_properties(Odometry PROPERTIES POSITION_INDEPENDENT_CODE ON)
+target_link_libraries(Odometry PRIVATE LibDepends)
 ShowAndAbortOnWarning(Odometry)
 
 # Enforce 3rd party dependencies
diff --git a/src/Open3D/Open3DConfig.cmake.in b/src/Open3D/Open3DConfig.cmake.in
index 9e66e8e..1f30369 100644
--- a/src/Open3D/Open3DConfig.cmake.in
+++ b/src/Open3D/Open3DConfig.cmake.in
@@ -1,14 +1,36 @@
-# Config file for the Open3D package, defines the following variables
-#  Open3D_INCLUDE_DIRS
-#  Open3D_LIBRARIES
-#  Open3D_LIBRARY_DIRS
-
 @PACKAGE_INIT@
+include(CMakeFindDependencyMacro)
+include(FindPackageHandleStandardArgs)
+
+# Find dependencies
+foreach(lib IN ITEMS @OPEN3D_3RDPARTY_PUBLIC_MODULES@)
+	find_dependency(${lib})
+endforeach()
+
+include("${CMAKE_CURRENT_LIST_DIR}/@PROJECT_NAME@Targets.cmake")
+
+set(@PROJECT_NAME@_FOUND TRUE)
+set(@PROJECT_NAME@_VERSION "@PROJECT_VERSION_MAJOR@.@PROJECT_VERSION_MINOR@.@PROJECT_VERSION_PATCH@")
+set(@PROJECT_NAME@_LIBRARIES "@PROJECT_NAME@::@PROJECT_NAME@")
+set_and_check(@PROJECT_NAME@_INCLUDE_DIRS "@PACKAGE_CMAKE_INSTALL_INCLUDEDIR@")
 
-set(Open3D_INCLUDE_DIRS "@PACKAGE_CONFIG_Open3D_INCLUDE_DIRS@")
-set(Open3D_LIBRARY_DIRS "@PACKAGE_CONFIG_Open3D_LIBRARY_DIRS@")
-set(Open3D_LIBRARIES    "@CONFIG_Open3D_LIBRARIES@")
+# Expose include paths to some library headers which are used in the @PROJECT_NAME@
+# public headers, but avoid linking against the libraries, as the headers do
+# not need the binary symbols.
+foreach(target IN ITEMS @OPEN3D_3RDPARTY_PUBLIC_HEADERS@)
+	get_property(inc TARGET ${target} PROPERTY INTERFACE_INCLUDE_DIRECTORIES)
+	if(inc)
+		set_property(TARGET @PROJECT_NAME@::@PROJECT_NAME@ APPEND PROPERTY INTERFACE_INCLUDE_DIRECTORIES ${inc})
+	endif()
+	get_property(def TARGET ${target} PROPERTY INTERFACE_COMPILE_DEFINITIONS)
+	if(def)
+		set_property(TARGET @PROJECT_NAME@::@PROJECT_NAME@ APPEND PROPERTY INTERFACE_COMPILE_DEFINITIONS ${def})
+	endif()
+	get_property(fea TARGET ${target} PROPERTY INTERFACE_COMPILE_FEATURES)
+	if(fea)
+		set_property(TARGET @PROJECT_NAME@::@PROJECT_NAME@ APPEND PROPERTY INTERFACE_COMPILE_FEATURES ${fea})
+	endif()
+endforeach()
 
-set(Open3D_C_FLAGS          "@Config_Open3D_C_FLAGS@")
-set(Open3D_CXX_FLAGS        "@Config_Open3D_CXX_FLAGS@")
-set(Open3D_EXE_LINKER_FLAGS "@Config_Open3D_EXE_LINKER_FLAGS@")
+set(@PROJECT_NAME@_CONFIG ${CMAKE_CURRENT_LIST_FILE})
+find_package_handle_standard_args(@PROJECT_NAME@ CONFIG_MODE)
diff --git a/src/Open3D/Registration/CMakeLists.txt b/src/Open3D/Registration/CMakeLists.txt
index 2e30b43..57dc975 100644
--- a/src/Open3D/Registration/CMakeLists.txt
+++ b/src/Open3D/Registration/CMakeLists.txt
@@ -3,6 +3,8 @@ file(GLOB_RECURSE ALL_SOURCE_FILES "*.cpp")
 
 # create object library
 add_library(Registration OBJECT ${ALL_SOURCE_FILES})
+set_target_properties(Registration PROPERTIES POSITION_INDEPENDENT_CODE ON)
+target_link_libraries(Registration PRIVATE LibDepends)
 ShowAndAbortOnWarning(Registration)
 
 # Enforce 3rd party dependencies
diff --git a/src/Open3D/Utility/CMakeLists.txt b/src/Open3D/Utility/CMakeLists.txt
index cb9ce11..e9b2bb0 100644
--- a/src/Open3D/Utility/CMakeLists.txt
+++ b/src/Open3D/Utility/CMakeLists.txt
@@ -3,6 +3,8 @@ file(GLOB_RECURSE ALL_SOURCE_FILES "*.cpp")
 
 # create object library
 add_library(Utility OBJECT ${ALL_SOURCE_FILES})
+set_target_properties(Utility PROPERTIES POSITION_INDEPENDENT_CODE ON)
+target_link_libraries(Utility PRIVATE LibDepends)
 ShowAndAbortOnWarning(Utility)
 
 # Enforce 3rd party dependencies
diff --git a/src/Open3D/Visualization/CMakeLists.txt b/src/Open3D/Visualization/CMakeLists.txt
index 2a40e62..ad3c7a5 100644
--- a/src/Open3D/Visualization/CMakeLists.txt
+++ b/src/Open3D/Visualization/CMakeLists.txt
@@ -31,6 +31,8 @@ add_library(Visualization OBJECT
             ${SHADER_FILES}
             ${VISUALIZATION_ALL_HEADER_FILES}
             ${VISUALIZATION_ALL_SOURCE_FILES})
+target_link_libraries(Visualization PRIVATE LibDepends)
+set_target_properties(Visualization PROPERTIES POSITION_INDEPENDENT_CODE ON)
 
 source_group("Source Files\\Shader\\GLSL" FILES ${SHADER_FILES})
 
diff --git a/src/Tools/CMakeLists.txt b/src/Tools/CMakeLists.txt
index 10e889f..40c3e20 100644
--- a/src/Tools/CMakeLists.txt
+++ b/src/Tools/CMakeLists.txt
@@ -5,15 +5,15 @@ macro(TOOL TOOL_NAME)
 
     set(DEPENDENCIES "${ARGN}")
     foreach(DEPENDENCY IN LISTS DEPENDENCIES)
-        target_link_libraries(${TOOL_NAME} ${DEPENDENCY})
+        target_link_libraries(${TOOL_NAME} PRIVATE ${DEPENDENCY})
     endforeach()
 
     set_target_properties(${TOOL_NAME} PROPERTIES FOLDER "Tools")
     ShowAndAbortOnWarning(${TOOL_NAME})
 endmacro(TOOL)
 
-TOOL(ConvertPointCloud      ${CMAKE_PROJECT_NAME})
+TOOL(ConvertPointCloud      ${CMAKE_PROJECT_NAME} HeaderDepends)
 TOOL(EncodeShader)
-TOOL(ManuallyCropGeometry   ${CMAKE_PROJECT_NAME})
-TOOL(MergeMesh              ${CMAKE_PROJECT_NAME})
-TOOL(ViewGeometry           ${CMAKE_PROJECT_NAME})
+TOOL(ManuallyCropGeometry   ${CMAKE_PROJECT_NAME} HeaderDepends)
+TOOL(MergeMesh              ${CMAKE_PROJECT_NAME} HeaderDepends)
+TOOL(ViewGeometry           ${CMAKE_PROJECT_NAME} HeaderDepends)
diff --git a/src/Tools/ManuallyAlignPointCloud/CMakeLists.txt b/src/Tools/ManuallyAlignPointCloud/CMakeLists.txt
index a7c531a..640dc8f 100644
--- a/src/Tools/ManuallyAlignPointCloud/CMakeLists.txt
+++ b/src/Tools/ManuallyAlignPointCloud/CMakeLists.txt
@@ -6,6 +6,6 @@ include_directories(".")
 add_executable(ManuallyAlignPointCloud ${HEADER_FILES} ${SOURCE_FILES})
 ShowAndAbortOnWarning(ManuallyAlignPointCloud)
 
-target_link_libraries(ManuallyAlignPointCloud ${CMAKE_PROJECT_NAME})
+target_link_libraries(ManuallyAlignPointCloud ${CMAKE_PROJECT_NAME} HeaderDepends jsoncpp_lib tinyfiledialogs)
 
 set_target_properties(ManuallyAlignPointCloud PROPERTIES FOLDER "Tools")
diff --git a/src/UnitTest/CMakeLists.txt b/src/UnitTest/CMakeLists.txt
index 91cee6c..202551f 100644
--- a/src/UnitTest/CMakeLists.txt
+++ b/src/UnitTest/CMakeLists.txt
@@ -19,5 +19,5 @@ endif()
 add_executable(unitTests ${UNIT_TEST_SOURCE_FILES})
 add_definitions(-DTEST_DATA_DIR="${PROJECT_SOURCE_DIR}/examples/TestData")
 
-target_link_libraries(unitTests pthread ${CMAKE_PROJECT_NAME})
+target_link_libraries(unitTests PRIVATE ${CMAKE_PROJECT_NAME} LibDepends 3rdparty::googletest)
 ShowAndAbortOnWarning(unitTests)
