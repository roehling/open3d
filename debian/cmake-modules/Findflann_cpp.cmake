include(FindPackageHandleStandardArgs)
find_package(PkgConfig REQUIRED)

pkg_search_module(pc_flann flann QUIET)

find_path(flann_cpp_INCLUDE_DIRS flann/flann.hpp HINTS ${pc_flann_INCLUDE_DIRS})

set(flann_cpp_LIBRARIES)
foreach(lib IN LISTS pc_flann_LIBRARIES)
	find_library(${lib}_LIBRARY ${lib} HINTS ${pc_flann_LIBRARY_DIRS})
	list(APPEND flann_cpp_LIBRARIES ${${lib}_LIBRARY})
endforeach()

add_library(3rdparty::flann_cpp INTERFACE IMPORTED)
set_target_properties(3rdparty::flann_cpp PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "${flann_cpp_INCLUDE_DIRS}"
	INTERFACE_LINK_LIBRARIES "${flann_cpp_LIBRARIES}"
)

find_package_handle_standard_args(flann_cpp DEFAULT_MSG flann_cpp_INCLUDE_DIRS flann_cpp_LIBRARIES)

