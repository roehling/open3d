include(FindPackageHandleStandardArgs)
set(googletest_DIR "/usr/src/googletest")
set(CMAKE_THREAD_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)

add_library(3rdparty::googletest INTERFACE IMPORTED)
set_target_properties(3rdparty::googletest PROPERTIES
	INTERFACE_INCLUDE_DIRECTORIES "${googletest_DIR}/googletest/include;${googletest_DIR}/googletest;${googletest_DIR}/googlemock/include;${googletest_DIR}/googlemock"
	INTERFACE_SOURCES "${googletest_DIR}/googletest/src/gtest-all.cc;${googletest_DIR}/googlemock/src/gmock-all.cc"
	INTERFACE_LINK_LIBRARIES "Threads::Threads"
)
find_package_handle_standard_args(googletest DEFAULT_MSG googletest_DIR)

